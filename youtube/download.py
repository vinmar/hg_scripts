from __future__ import unicode_literals
import youtube_dl
import sys, subprocess
import multiprocessing
import subprocess
from multiprocessing.pool import ThreadPool as Pool
from multiprocessing import Lock
import os
import time

NUMBER_OF_PROCESS = int(multiprocessing.cpu_count())

lock = Lock()

links = []


def output(*args):
    msg = ' '.join(str(x) for x in args)
    msg = '%s%s' % (msg, os.linesep)
    with lock:
        sys.stdout.write(msg)
        sys.stdout.flush()


def run(link):
    current = multiprocessing.Process()
    start_time = time.time()
    output(current._identity, "[Begin]",link)
    command = ['youtube-dl','-x','--audio-format','mp3',link]
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()
    return_code = proc.returncode
    #print multiprocessing.current_process()._identity,"[Done]",out,err
    elapsed_time = time.time() - start_time
    output(current._identity, "[Finish]", link, "in", elapsed_time, 's')
    #return out+err, (return_code == success_return_code)

with open("links.txt") as f:
    links = f.readlines()

class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')

ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}


print "Downloading [processes: "+str(NUMBER_OF_PROCESS)+"]"
stripped_list = list()

# populate the queue
for l in links:
    stripped_list.append(l.strip())


pool = Pool(NUMBER_OF_PROCESS)
num_links = float(len(stripped_list))
count = 0
for result in pool.imap_unordered(run, stripped_list):
    count += 1
    output('[Progress]', str(count/num_links*100)+'%')


print "Finish"
