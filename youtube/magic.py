#!/usr/bin/python

import os

LINK_FILE = "links.txt"

with open(LINK_FILE,'w') as f:
	pass


def escape(string):
	string = string.strip()
	string = string.replace('"','\\"')
	return '"'+string+'"'

print "======================"
print "  SEARCH"
print "======================"
with open("search.txt") as search_file:
  lines = search_file.readlines()
  for l in lines:
    l = escape(l)
    print l
    os.system("python search.py --q "+l.strip()+" --s links.txt")

print "======================"
print "  DOWNLOAD"
print "======================"
os.system("python download.py")
