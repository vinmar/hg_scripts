#!/usr/bin/python

#easy_install romkan
from __future__ import unicode_literals

import re
import romkan
import csv
import codecs

HIRROM_H = {}
ROMHIR_H = {}

WORD_RH = {}

SELECTION = {}

def pairs(arr, size=2):
    for i in range(0, len(arr)-1, size):
        yield arr[i:i+size]

def init():
	f = open('japanese_jlpt1_lite.csv', 'rb')
	reader = csv.reader(f)
	count = 0;
	for utf8_row in reader:
		romaji = utf8_row[2] 
		hiragana = utf8_row[1]
		#[hiragana] = romanji
		WORD_RH[romaji] = hiragana
	
		
	for pair in pairs(re.split("\s+",  romkan.HEPBURNTAB_H)):
		roma, hira = pair
		HIRROM_H[roma] = hira
		
	for pair in pairs(re.split("\s+",  romkan.HEPBURNTAB_H)):
		roma, hira = pair
		ROMHIR_H[hira] = roma
	
def searchR(arrayRoma):
	arrayHira = []
	for r in arrayRoma:
		arrayHira.append(romkan.to_hiragana(r))
	print WORD_RH
	
	'''
	for word in WORD_RH:
		print WORD_RH[word], " = ",word 
	
		count = 0;
		for w in word:
			if w in arrayHira
				count = count +1
		if count == len(word):
			SELECTION[roma] = word
	'''
init()
print romkan.to_hiragana("ninja").encode('utf-8')

'''
jstr = u'\u30b0\u30ec\u30fc'.decode('utf-16').encode('utf-8')
print jstr
print jstr.encode('iso-2022-jp')
print jstr.encode('euc-jp')
print jstr.encode('euc-jisx0213')
print jstr.encode('euc-jis-2004')
print jstr.encode('iso-2022-jp')
print jstr.encode('iso-2022-jp-1')
print jstr.encode('iso-2022-jp-2')
print jstr.encode('iso-2022-jp-3')
print jstr.encode('iso-2022-jp-ext')
print jstr.encode('iso-2022-jp-2004')

print jstr.encode('utf-7')
print jstr.encode('utf-8')
print jstr.encode('utf-16')
print jstr.encode('utf-16-be')
print jstr.encode('utf-16-le')

print jstr.encode('cp932')      
print jstr.encode('shift-jis')  
print jstr.encode('shift-jisx0213') 
print jstr.encode('shift-jis-2004') 

#var = raw_input("Enter the romanji separated by comma ( eg: a, ka, shi ): ")
#print "You entered ", var
#arra_var = var.split(',');
#searchR(arra_var)
'''
'''
print "Found ", len(SELECTION) , " words" 
for w in SELECTION:
	print "> ", w
'''