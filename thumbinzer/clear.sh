#!/bin/bash

for file in `ls`; do
      if [ -d $file ]
      then
              # do something directory-ish
				echo $file ": is a Directory"
      else
              if [[ ${file: -4} == ".jpg" ]]       #  this is the snag
              then
                   rm -f ${file}
              fi

              if [[ ${file: -4} == ".png" ]]       #  this is the snag
              then
                   rm -f ${file}
              fi
      fi
done;

cp original/* .
rm output/*
rm -rf output
