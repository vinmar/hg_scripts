#!/bin/bash



function generateRandomAngle
{	
	local abs_value=$(($RANDOM%15))
	local sign=$(($RANDOM%2))
	if [ $sign == "1" ]
	then
		echo -$abs_value
	else
		echo $abs_value
	fi
}

function border {
	

	local bname=$(basename ${1})
	bname=${bname%.*}
	local file=`echo ${bname}-bord.png`
	convert -border 9x9 -bordercolor "#FFFFFF" ${1} ${file}
	
	echo "${file}"
	
}

function shadow {


	local bname=$(basename ${1})
	bname=${bname%.*}				
	local file=`echo ${bname}-sha.png`
	convert ${1} \( +clone -background none -shadow 58x8+5+5 \) +swap \
	          -background none  -layers merge +repage  ${file}
	
	echo "${file}"
	
}

function rotate {
	
	local bname=$(basename ${1})
	bname=${bname%.*}				
	local file=`echo ${bname}-rot.png`
	angle=$(generateRandomAngle)
	
	convert ${1} -background none -rotate $angle ${file}
	
	echo "${file}"
}

function resize {
	

	local bname=$(basename ${1})
	bname=${bname%.*}				
	local file=`echo ${bname}-res.png`
	convert ${1} -resize 200x200 ${file}
	
	echo "${file}"
	
}



for file in `ls`; do
      if [ -d $file ]
      then
              # do something directory-ish
				echo $file ": is a Directory"
      else
              if [[ ${file: -4} == ".jpg" ]]       #  this is the snag
              then
                  
  					echo "Processing: ["${file}"]"

					echo "--> resize to max 200x200"
					file_resized=$(resize ${file})

					echo "--> create borders around the image"
					file_border=$(border ${file_resized})				
					
					echo "--> add shadow"
					file_shadow=$(shadow ${file_border})
					
					echo "--> rotate image of random degree"
					file_rotated=$(rotate ${file_shadow})
					mkdir output
					cp ${file_rotated} output
              fi
      fi
done;
