#!/bin/bash



currDir=`pwd`;
cd "$1"
for i in $(ls); do
	
	rot_img="rot_`basename $i`.png"
	thumb_img="thumb_`basename $i.png`"
	png_img="`basename $i`.png" 

	#add transparent background & png
	convert $i -format PNG32 -background transparent $png_img
		
	
	# Rotate image
	#convert -rotate `jot -r 1 -8 8` border_$i $rot_img
	
	# Resize image
	convert -scale 214x190  $png_img $thumb_img
	

			  convert $thumb_img \
			          -bordercolor white  -border 6 \
			          -bordercolor grey60 -border 1 \
			          -background  none   -rotate `jot -r 1 -8 8` \
			          -background  black  \( +clone -shadow 60x4+4+4 \) +swap \
			          -background  none   -flatten \
			          border_$thumb_img
	
	
	#add border
	#convert $thumb_img -border 1x1 border_$thumb_img
	


	
done

cd $currDir