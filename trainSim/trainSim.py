import globalvar
import time, threading
from configurationServer import ConfigurationServer
import csv
import passengerModel
import datetime
from BaseHTTPServer import HTTPServer
import signal
import sys

def signal_handler(signal, frame):
	print 'You pressed Ctrl+C!'
	print '..terminating...'
	joinTrains()
	sys.exit(0)

def loadTrains():
	'''
	1- parse the trainfolder:
	 trains/
			3456.xml
			22343.xml
			....
			train_number.xml
	2- fill the train model
	'''
	return []


def loadPassangersOn(trains):
	globalvar.gPassengerThreads = []
	#read passenger file
	with open('data/passengers.csv', 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			paxId = row[0]
			onTrain = row[1]
			requestHz = row[2]
			#if onTrain in trains:
			pt = passengerModel.PassengerInstance(paxId, onTrain, float(requestHz))
			globalvar.gPassengerThreads.append(pt)

def startTrains():
	for p in globalvar.gPassengerThreads:
		p.start()
		
def joinTrains():
	for p in globalvar.gPassengerThreads:
		p.go()
	for p in globalvar.gPassengerThreads:
		p.join()

def run():
	#create a first thread in charge to handle the configuration message

	print "...load trains"
	trains = loadTrains()
	print "...load passengers"
	passengerThreads = loadPassangersOn(trains)
	print "...start trains"
	startTrains()
	time.sleep(2)
	print "...start server, use <Ctrl-C> to stop"
	server = HTTPServer(('localhost', 8080), ConfigurationServer)
	server.serve_forever()

	
	
print "Rail traffic simulator"
signal.signal(signal.SIGINT, signal_handler)
run()