from BaseHTTPServer import BaseHTTPRequestHandler
import urlparse
import json

import globalvar

class ConfigurationServer(BaseHTTPRequestHandler):
	

	def do_GET(self):
		parsed_path = urlparse.urlparse(self.path)
		c, p = self.decodeQuery(parsed_path.query)
		result = self.executeCommand(c,p)
		self.send_response(200)
		self.send_header('Access-Control-Allow-Origin', '*')
		self.end_headers()
		message = self.encodeReplay(result)
		self.wfile.write(message)
		return
		
	def encodeReplay(self, result):
		return json.dumps(result)
	
	def decodeQueryElement(self, elem):
		splittedElem = elem.split('=')
		if ( len(splittedElem) > 1):
			return splittedElem[1]
		else:
		    return "-1"
		
	def decodeQuery(self, query):
		c = 0
		p = 0
		try:
			splittedQuery = query.split('&')
			if (len(splittedQuery) > 0):
				c=self.decodeQueryElement(splittedQuery[0])
			if (len(splittedQuery) > 1):
				p=self.decodeQueryElement(splittedQuery[1])
		except:
			c=-1
			p=-1
		return c,p
		
		
	def executeCommand(self,code, parameter):
		code=int(code)
		parameter=int(parameter)
		
		if(code == 3):
			self.setSpeed(parameter)
			return "OK",""
		if(code == 0):
			self.stop()
			return "OK",""
		if(code == 1):
			self.go()
			return "OK",""
		if(code == 2):
			self.pause()
			return "OK",""
		return ["KO","UNKNOWN COMMAND"]
		
	def setSpeed(self, value):
		globalvar.gSimulationSpeedSpin = value
		return
	
	def go(self):
		# release lock
		# and start thread
		for pt in globalvar.gPassengerThreads:
			pt.go()
		return
		
	def stop(self):
		for pt in globalvar.gPassengerThreads:
			pt.stop()
		return
		
	def pause(self):
		for pt in globalvar.gPassengerThreads:
			pt.pause()
		return
		