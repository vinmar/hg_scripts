import time, threading
import json
import datetime


class PassengerInstance(threading.Thread):
	def __init__(self, id, trainInstance, hz ):
		super(PassengerInstance,self).__init__()
		print "...PassengerInstance start"
		# link passenger thread - train instance
		self.id  = id
		self.reqHz = hz
		self.trainInstance = trainInstance
		self.pauserequest = threading.Event()
		self.shutdownrequest = threading.Event()
		self.gorequest = threading.Event()
		self.dbgCounter = 0
		
	def resetState(self):
		self.dbgCounter = 0
		
	def handleEvents(self):
		if self.pauserequest.isSet() or self.stoprequest.isSet() :
			self.gorequest.clear()
			self.gorequest.wait()
			self.pauserequest.clear()
			self.stoprequest.clear();
			
	def run(self):
		while  not self.shutdownrequest.isSet():
			self.handleEvents()
			
			print "[",datetime.datetime.now(),"] ", self.id, " -> sendDelayRequest(",self.dbgCounter,") on ",self.trainInstance 
			self.dbgCounter = self.dbgCounter+1
			time.sleep(self.reqHz)
		'''
		loadCOnfiguration, assume request HZ = 10 minutes
		arrived = False
		while (not arrived):
			getPosition()
			getTime()
			sendDelayRequest()
			self.saveState()
			waitforNextTick()
		'''
	
	def go(self):
		if not self.gorequest.isSet():
			self.gorequest.set()

	def pause(self):
		if not self.pauserequest.isSet():
			self.pauserequest.set()
	
	def stop(self):
		if not self.stoprequest.isSet():
			self.stoprequest.set()
			self.resetState()
						
	
	def join(self, timeout=None):
		self.shutdownrequest.set()
		super(PassengerInstance, self).join(timeout)
		