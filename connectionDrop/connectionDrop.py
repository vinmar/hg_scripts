'''
usage 'connectionDropp port host [newport]'

connectionDrop forwards the port to the host specified.
The optional newport parameter may be used to
redirect to a different port.

eg. connectionDrop 80 webserver
    Forward all incoming WWW sessions to webserver.

    connectionDrop 23 localhost 2323
    Forward all telnet sessions to port 2323 on localhost.
'''

import psutil
import sys
from socket import *
from threading import Thread
from threading import Event
import time
import signal
import getpass
from optparse import OptionParser

LOGGING = 1
pipesHandlers = []

def log( s ):
    if LOGGING:
        print '%s:%s' % ( time.ctime(), s )
        sys.stdout.flush()

def getProcesses(oProcess):
    for proc in psutil.process_iter():           
        if proc.username == getpass.getuser():                  
            oProcess[str(proc.pid)+"#"+str(proc.name)+"#"+str(proc.cmdline)] = proc            

def checkProcesses(iAProcess, iBProcess, oDiff):
    added, removed, same = dict_compare(iAProcess, iBProcess)
    for i in removed:
        oDiff.append(i)

    return len(same) == len(iAProcess)

def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d2_keys - d1_keys
    removed = d1_keys - d2_keys
    #modified = {o : (d1[o], d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return added, removed, same

#print aProcess == bProcess


def stopForward(c, port, initialProcess, oldstdout):
    log('--- stop forwarding')
    c.stop()
    closingSock = socket( AF_INET, SOCK_STREAM )
    closingSock.connect(( '', port ))
    closingSock.shutdown(1)
    closingSock.close()
    time.sleep(2)
    log('--- check processes')
    getProcesses(afterProcess) 
    sys.stdout = oldstdout
    if ( checkProcesses(initialProcess, afterProcess, aDiff) != True ):
        print "FAIL"
        for p in aDiff:
            print p
        sys.exit(1)
    else:
        print "SUCCESS"
        sys.exit(0)


class PipeThread( Thread ):
    pipes = []
    def __init__( self, source, sink ):
        Thread.__init__( self )
        self.setDaemon(True)
        self.source = source
        self.sink = sink
        self._stop = Event()
        log( 'Creating new pipe thread  %s ( %s -> %s )' % \
            ( self, source.getpeername(), sink.getpeername() ))
        PipeThread.pipes.append( self )
        pipesHandlers.append(self)
        log( '%s pipes active' % len( PipeThread.pipes ))

    def run( self ):
        while 1:
            try:                
                data = self.source.recv( 1024 )
                if (self.stopped()):                
                    break
                if not data: break
                self.sink.send( data )
            except:
                break

        log( '%s terminating' % self )
        PipeThread.pipes.remove( self )        
        log( '%s pipes active' % len( PipeThread.pipes ))

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
        
class connectionDrop( Thread ):
    def __init__( self, port, newhost, newport ):
        #Thread.__init__( self )
        super(connectionDrop, self).__init__()
        self._stop = Event()
        log( 'Redirecting: localhost:%s -> %s:%s' % ( port, newhost, newport ))
        self.newhost = newhost
        self.newport = newport
        self.sock = socket( AF_INET, SOCK_STREAM )
        self.sock.bind(( '', port ))
        self.sock.listen(5)
        self.sockets=[]        

    def run( self ):
        while 1:
            newsock, address = self.sock.accept()            
            self.sockets.append(newsock)
            if (self.stopped()):                               
                return

            log( 'Creating new session for %s %s ' % address )
            fwd = socket( AF_INET, SOCK_STREAM )
            try: 
                fwd.connect(( self.newhost, self.newport ))                
                
                PipeThread( newsock, fwd ).start()
                PipeThread( fwd, newsock ).start()                                        

            except: 
                log( 'Not reachable  %s %s ' % address )
                break
                
    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
       
if __name__ == '__main__':

    parser = OptionParser(usage="usage: %prog [options] ",
                              version="%prog 1.0")
    parser.add_option("-l", "--local",
                          action="store",
                          dest="port",
                          default=0,
                          help="port to forward from")
    parser.add_option("-f", "--host",
                          action="store",
                          dest="host",
                          default=0,
                          help="host to forward to")
    parser.add_option("-p", "--port",
                          action="store",
                          dest="newport",
                          default=0,
                          help="port to forward to")
    parser.add_option("-t", "--timeout",
                          action="store", # optional because action defaults to "store"
                          dest="timeout",
                          default=-1,
                          help="forwarding timeout")

    (options, args) = parser.parse_args()



    if not options.port:
        parser.print_help()
        parser.error("missing mandatory parameter")

    if not options.newport:
        parser.print_help()
        parser.error("missing mandatory parameter")
    
    if not options.host:
        parser.print_help()
        parser.error("missing mandatory parameter")

    port = int(options.port)
    newport = int(options.newport)
    newhost = options.host
    timeout = int(options.timeout)
    if timeout == -1:
        timeout = 300 

    print 'Starting connectionDrop'
    print ' localhost:'+str(port)+' -> '+str(newhost)+':'+str(newport)    
    print 'Wait '+str(timeout)+' [s] or press CTRL-C to stop'

    oldstdout = sys.stdout
    sys.stdout = open( 'connectionDrop.log', 'w' )
    

    log('--- start forwaring')
    
    c = connectionDrop( port, newhost, newport )

    c.start()
    initialProcess = dict()
    afterProcess = dict()
    aDiff =  list()
    getProcesses(initialProcess)

    def signal_handler(*args):
        tmpstd = sys.stdout
        sys.stdout = oldstdout
        print "Manual stop, please be patient..."
        sys.stdout = tmpstd
        stopForward(c, port, initialProcess, oldstdout)
        sys.exit()
    signal.signal(signal.SIGINT, signal_handler) # CTRL-C

    time.sleep(timeout)
    stopForward(c, port, initialProcess, oldstdout)    


