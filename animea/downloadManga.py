#!/usr/bin/env python

import os, fnmatch
import sys
import signal
from urllib2 import urlopen
from BeautifulSoup import BeautifulSoup
import urllib

#
# http://manga.animea.net/noritaka-chapter-1-page-1.html
# 
#
#

def processUrl(url):
	req = urlopen(url)
	html = req.read()
	soup = BeautifulSoup(html)
	return soup

def getNumberOfChapters(soap):
	return 61+1
	print soap
	#select = soup.find('select',attrs={'class':'mangaselecter pageselect'})
	select = soap.find('select',attrs={'name':'chapter'})
	print select
	option_tags = select.findAll('option')
	print option_tags
	
	return option_tags
	
def getNumberOfPages(soap):
	select = soap.find('select',attrs={'class':'mangaselecter pageselect'})
	if ( select ):
		return len(select)
	else:
		return 0
	option_tags = select.findAll('option')
	if ( option_tags ):
		return len(option_tags)
	else:
		return 0
	
def getImageUrl(soap):
	img = soap.find('img',attrs={'class':'mangaimg'})
	return img["src"]

def toPatternString(value, number_of_digit):
	zero_to_be_added = number_of_digit - len(str(value))
	out_str = ""
	for i in range(zero_to_be_added):
		out_str+="0"
	out_str += str(value)
	return out_str

def generateDestinationPathName(manga_name,path, c, p):
	out_str = path
	out_str += "/"+manga_name
	out_str += "_"+toPatternString(c, 3)
	out_str += "_"+toPatternString(p, 4)
	out_str += ".jpg"
	return out_str

def downloadImage(imageUrl, destinationPathName):
	image=urllib.URLopener()
	image.retrieve(imageUrl,destinationPathName)  # download comicName at URL
	return

	# http://manga.animea.net/noritaka-chapter-1-page-1.html
def generateUrl(manga_name, c, p):
	out_str = "http://manga.animea.net/"
	out_str += manga_name
	out_str += "-"
	out_str += "chapter"
	out_str += "-"
	out_str += str(c)
	out_str += "-"
	out_str += "page"
	out_str += "-"
	out_str += str(p) 
	out_str += ".html"
	return out_str

#imageurl = processPage("http://manga.animea.net/noritaka-chapter-1-page-1.html")
manga_name = "noritaka"
c = 1
p = 1

url = generateUrl(manga_name, c, p);
soap = processUrl(url)
#hard coded...
numberOfChapters = getNumberOfChapters(soap)
path = "./download" 

if not os.path.exists(path):
    os.makedirs(path)

for c in range(59,numberOfChapters):
	
	chapterUrl = generateUrl(manga_name, c, p);
	soap = processUrl(chapterUrl)
	pages = getNumberOfPages(soap)
	
	for p in range(1,pages-1):
		try:
			print "Processing Chapter ["+toPatternString(c, 3)+"], Page ["+toPatternString(p,4)+"]"
			pageUrl = generateUrl(manga_name, c, p);
			print " ---> Parsing "+pageUrl,
			soap = processUrl(pageUrl)
			print "... Done"
		
			print " ---> Get image URL ",
			imageUrl = getImageUrl(soap)
			print " ... Done"
		
			destinationPathName = generateDestinationPathName(manga_name,path, c, p)
			print " ---> Downloading to "+destinationPathName, 
			downloadImage(imageUrl, destinationPathName)
			print "... Done"
		except:
			pass