#!/usr/bin/python

#write to disk
#when the file reaches a given dimension D create a new one
#when we have N files zip them and mode to an archive folder
#after time T remove file from archive ( not implemented )

#log file format
#[usertag].log ( symbolic link )
#[start_w3cdateTtime]_[usertag].log
#[start_w3cdateTtime]_[usertag].log
#[start_w3cdateTtime]_[usertag].log
#i.e. ddmmyyyThhmmss_test.log

#log line format
#[w3cdatetime] [author] [type] [text]
#i.e. [ddmmyyyThhmmss.uuu][SERVER][INFO]: text text text
#i.e. [ddmmyyyThhmmss.uuu][SERVER][WARN]: text text text
#i.e. [ddmmyyyThhmmss.uuu][SERVER][ERRO]: text text text 
#i.e. [ddmmyyyThhmmss.uuu][SERVER][DEBG]: text text text
#i.e. [ddmmyyyThhmmss.uuu][SERVER][NONE]: text text text

#type of logs
#INFO violet: 
#WARN yellow: 
#ERRO red: 
#DEBG blue: used for dev purpose
#NONE white: 

import datetime
import zlib
import glob
import os
from threading import Thread
import zipfile
import sys
from time import sleep


class bcolors:
	INFO = '\033[95m'
	DEBG = '\033[92m'
	NONE = '\033[0m'
	WARN = '\033[94m'
	ERRO = '\033[91m'
	ENDC = '\033[0m'
	
	def getColorFromType(self, iType):
		if ( iType == "INFO" ):
			return self.INFO
		if ( iType == "DEBG" ):
			return self.DEBG
		if ( iType == "WARN" ):
			return self.WARN
		if ( iType == "ERRO" ):
			return self.ERRO
		return self.NONE
		
	def endColor(self):
		return self.ENDC
	
	def disable(self):
		self.INFO = ''
		self.DEBG = ''
		self.NONE = ''
		self.WARN = ''
		self.ERRO = ''
		self.ENDC = ''
	

class Logger:
	#Input:
	# - ifolder, folder
	# - iDimension, max dimension [MB]
	# - iNumber, number of file
	# - iTime, time[h]
	def __init__(self, iFolder, iTag = ""):
		self.folder = iFolder
		self.userTag = iTag
		self.dimension = 0.1
		self.number = 0
		self.time = -1
		self.fullFilePath = ""
		self.filePointer = 0
		self.folder = iFolder
		self.userTag = iTag
		if not os.path.exists(self.folder):
			os.makedirs(self.folder)
		now = datetime.datetime.utcnow()
		self.fullFilePath = self.folder+"/"+str(now).replace(" ", "_")
		if len(self.userTag) > 0:
			self.fullFilePath+="_"+iTag
		self.fullFilePath += ".log"
		self.filePointer = open (self.fullFilePath, "a+")
		self.archive = self.folder+"/archive"
		self.threadZip = Thread();
		
	def doCreateNewLogFile(self):
		#close in case the old filepointer
		self.filePointer.close();
		
		#create a new file name
		now = datetime.datetime.utcnow()
		self.fullFilePath = self.folder+"/"+str(now).replace(" ", "_")
		if len(self.userTag) > 0:
			self.fullFilePath+="_"+iTag
		self.fullFilePath += ".log"
		
		#open the new file ( and create in case)
		self.filePointer = open (self.fullFilePath, "a+")
		
	def archiveLogs(self, logList):
		sleep(10)
		print "archiveLogs"
		#zip the log files
		try:
		    import zlib
		    compression = zipfile.ZIP_DEFLATED
		except:
		    compression = zipfile.ZIP_STORED
		#create the archive folder in case
		if not os.path.exists(self.archive): 
			os.makedirs(self.archive)
		#move the zipped file into the archive folder
		for l in logList:
			compressed = l+'.zip'
			zf = zipfile.ZipFile(compressed, mode='w')
			try:
			    zf.write(l, compress_type=compression)
			finally:
			    zf.close()
			
			os.system('mv '+compressed+' '+self.folder+'/archive/'+compressed.replace(self.folder+'/', ""))
			#remove the log files 
			os.system('rm -f '+l)
		
	
	def doChecks(self):
		#each action  ( apart the creation of a new log file )
		#following a check should be executed in a separate thread
		#check file dimension => create a new one
		if (self.filePointer.tell()/1024/1024) > self.dimension:
			self.doCreateNewLogFile()
		#check number of file created
		logList = glob.glob(self.folder+'/*.log') 
		
		# remove the current log file from the list
		logList.remove(self.fullFilePath)

		if len (logList) > self.number:
			if not self.threadZip.isAlive():
				print "Thread IS Alive"
				self.threadZip = Thread(target=self.archiveLogs, args=(logList,))
				self.threadZip.start()

		
	def log(self, iAuthor, iType, iText):
		aLine = self.buildLine(iAuthor, iType, iText)
		self.write(aLine+"\n")
		self.doChecks()
		#self.filePointer = open (self.fullFilePath, "r")
		#print self.filePointer.tell()
		#sys.stdout.write(".")
		
	def write(self, iLine):
		self.filePointer.write(iLine)
		
	def buildLine(self, iAuthor, iType, iText):
		bc = bcolors()
		aColorTypeStart = bc.getColorFromType(iType)
		aColorTypeEnd = bc.endColor()
		now = datetime.datetime.utcnow()
		#i.e. [ddmmyyyThhmmss.uuu][SERVER][INFO]: text text text
		l=aColorTypeStart
		l+="["+str(now).replace(" ", "_")+"]"
		l+="["+iAuthor+"]"
		l+="["+iType+"]: "
		l+=iText
		l+=aColorTypeEnd
		return l

l = Logger("log")

for n in range (0, 1000):
	for j in {"SERVER", "192.178.0.1", "Timer", "function1", "Clien4", "Pippo", "giovanni"}:
		for i in {"INFO", "DEBG", "NONE", "WARN", "ERRO", "", ".oki"}:
			l.log(j, i, "freetextttttt")
